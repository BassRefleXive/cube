<?php

foreach (range(1, 100) as $i) {
    echo implode('', array_merge(
        $i % 3 === 0 ? ['CUBE'] : [],
        $i % 5 === 0 ? ['Systems'] : []
    ));
}